import Author from 'sukopon/solid/Author.solid';
import Content from 'sukopon/solid/atoms/Content.solid';
import NestedComments from 'sukopon/solid/NestedComments.solid';

import type Props from 'sukopon/types/components/Comment';

const Comment = ({ comment }: Props) => (
	<div class='comment'>
		<Author comment={comment} />
		<Content comment={comment} />
		<NestedComments comments={comment.comments} />
	</div>
);

export default Comment;

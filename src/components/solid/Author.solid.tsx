import AuthorHandle from 'sukopon/solid/atoms/AuthorHandle.solid';
import AuthorName from 'sukopon/solid/atoms/AuthorName.solid';
import Avatar from 'sukopon/solid/atoms/Avatar.solid';
import Date from 'sukopon/solid/atoms/Date.solid';

import type Props from 'sukopon/types/components/Author';

const Author = ({ comment }: Props) => (
	<header class='author'>
		<Avatar author={comment.author} />

		<div class='author-text'>
			<AuthorName author={comment.author} />

			<div class='author-subheader'>
				<AuthorHandle author={comment.author} />
				<Date comment={comment} />
			</div>
		</div>
	</header>
);

export default Author;

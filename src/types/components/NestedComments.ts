import type CommentType from 'sukopon/types/comment';

export default interface Props {
	comments: CommentType[];
}

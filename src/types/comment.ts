import type Author from './author';

type Comment = {
	// ID is used internally
	id: string;
	url: string;
	author: Author;
	content: string;
	date: Date;
	comments: Comment[];
};

export default Comment;

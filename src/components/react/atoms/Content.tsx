import type Props from 'sukopon/types/components/atoms/Content';

const Content = ({ comment }: Props) => (
	<div
		className='content'
		dangerouslySetInnerHTML={{
			__html: comment.content
		}}
	/>
);

export default Content;

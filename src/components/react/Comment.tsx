import Author from 'sukopon/react/Author';
import Content from 'sukopon/react/atoms/Content';
import NestedComments from 'sukopon/react/NestedComments';

import type Props from 'sukopon/types/components/Comment';

const Comment = ({ comment }: Props) => (
	<div className='comment'>
		<Author comment={comment} />
		<Content comment={comment} />
		<NestedComments comments={comment.comments} />
	</div>
);

export default Comment;

import type Data from 'sukopon/types/data';
import type MastodonContext from 'sukopon/types/mastodon/context';
import type MastodonStatus from 'sukopon/types/mastodon/status';
import type Options from 'sukopon/types/options';
import constructComments from 'sukopon/api/mastodon/constructComments';

const request = async (statusId: string, options: Options): Promise<Data> => {
	const contextResponse = await fetch(`https://${options.instance}/api/v1/statuses/${statusId}/context`);
	const statusResponse = await fetch(`https://${options.instance}/api/v1/statuses/${statusId}`);
	const status: MastodonStatus = await statusResponse.json();
	const contextData: MastodonContext = await contextResponse.json();

	const comments = constructComments(statusId, contextData, options.instance);

	return {
		id: statusId,
		comments,
		link: status.url
	};
};

export default request;

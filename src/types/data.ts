import type Comment from './comment';

type Data = {
	id: string;
	link: string;
	comments: Comment[];
};

export default Data;

import type Comment from 'sukopon/types/comment';
import type Context from 'sukopon/types/mastodon/context';

// Recursively hunt for a comment
const findComment = (comments: Comment[], id: string): Comment | undefined => {
	for (const comment of comments) {
		if (comment.id === id) return comment;
		if (comment.comments.length > 1) {
			const foundComment = findComment(comment.comments, id);
			if (foundComment) return foundComment;
		}
	}

	return undefined;
};

const getHandle = (acct: string, instance: string) => `@${acct.includes('@') ? acct : `${acct}@${instance}`}`;

const constructComments = (statusId: string, postData: Context, instance: string): Comment[] => {
	const comments: Comment[] = [];
	// Many replies will be to the previous post, so we can speed it up a little by storing a reference to the previous comment
	let lastComment: Comment | undefined = undefined;

	postData.descendants.forEach(post => {
		const comment: Comment = {
			author: {
				name: post.account.display_name || post.account.username,
				handle: getHandle(post.account.acct, instance),
				link: post.account.url,
				avatar: post.account.avatar,
				emojis: post.account.emojis.map(emoji => ({
					shortcode: emoji.shortcode,
					url: emoji.url
				}))
			},
			id: post.id,
			date: new Date(post.created_at),
			url: post.url,
			comments: [],
			content: post.content
		};

		if (post.in_reply_to_id === statusId)
			comments.push(comment);
		else if (lastComment && lastComment.id === post.in_reply_to_id)
			lastComment.comments.push(comment);
		else {
			const found = findComment(comments, post.in_reply_to_id);

			// Federation is weird. Just ignore posts with no registered parent.
			if (found) found.comments.push(comment);
		}

		lastComment = comment;
	});

	return comments;
};

export default constructComments;

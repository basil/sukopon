import Layer from 'sukopon/react/atoms/Layer';
import Comment from 'sukopon/react/Comment';

import type Props from 'sukopon/types/components/NestedComments';

const NestedComments = ({ comments }: Props) =>
	!!comments.length && (
		<Layer>
			{comments.map((comment) => (
				<Comment
					key={comment.id}
					comment={comment}
				/>
			))}
		</Layer>
	);

export default NestedComments;

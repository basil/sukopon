import type { JSXElement } from 'solid-js';

type Props = {
	children: JSXElement;
}

const Layer = ({ children }: Props) => (
	<div class='layer'>
		{children}
	</div>
);

export default Layer;

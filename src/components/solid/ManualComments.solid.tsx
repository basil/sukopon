import Comment from 'sukopon/solid/Comment.solid';

import type Props from 'sukopon/types/components/ManualComments';

const ManualComments = ({ data }: Props) => (
	<>
		<div class='comments'>
			{data.comments.map((comment) => <Comment comment={comment} />)}
		</div>
		<a class='original-post' href={data.link}>View original post</a>
	</>
);

export default ManualComments;

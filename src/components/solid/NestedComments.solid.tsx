import Layer from 'sukopon/solid/atoms/Layer.solid';
import Comment from 'sukopon/solid/Comment.solid';

import type Props from 'sukopon/types/components/NestedComments';

const NestedComments = ({ comments }: Props) =>
	!!comments.length && (
		<Layer>
			{comments.map((comment) => <Comment comment={comment} />)}
		</Layer>
	);

export default NestedComments;

import type Data from 'sukopon/types/data';

export default interface Props {
	data: Data;
}

import type Props from 'sukopon/types/components/atoms/Content';

const Content = ({ comment }: Props) => (
	<div
		class='content'
		innerHTML={comment.content}
	/>
);

export default Content;

import type Comment from 'sukopon/types/comment';

export default interface Props {
	comment: Comment;
}

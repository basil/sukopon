import type Props from 'sukopon/types/components/atoms/Avatar';

const Avatar = ({ author }: Props) => (
	<img class='author-avatar' src={author.avatar} />
);

export default Avatar;

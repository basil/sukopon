import type Options from 'sukopon/types/options';

export default interface Props {
	status: string;
	options: Options;
}

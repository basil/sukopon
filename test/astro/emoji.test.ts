import { expect, test } from 'vitest';
import type Emoji from 'sukopon/types/emoji';
import initAstroComponent from './initAstroComponent';

const emoji: Emoji = {
	shortcode: 'chikutaku',
	url: 'https://boombox.basil.cafe/api/cover/948'
};

test('astro emoji component', async () => {
	await initAstroComponent('./src/components/react/atoms/Emoji.tsx', { emoji })
	const img = document.querySelector('img.emoji');

	expect(img, 'image element is found by class').toBeDefined();
	expect(img.getAttribute('src')).eq(emoji.url, 'image src is from emoji');
	expect(img.getAttribute('alt')).eq(emoji.shortcode, 'image alt tag is shortcode');
	expect(img.getAttribute('title')).eq(emoji.shortcode, 'image title tag is shortcode');
});
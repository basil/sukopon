import { useState, useEffect } from 'react';
import getComments from 'sukopon/api/mastodon/getComments';
import ManualComments from 'sukopon/react/ManualComments';

import type Props from 'sukopon/types/components/Comments';
import type Data from 'sukopon/types/data';

const Comments = ({ status, options }: Props) => {
	const [data, setData] = useState<Data>();
	
	useEffect(() => {
		(async () => {
			const data = await getComments(status, options);
			setData(data);
		})();
	}, []);

	return (!!data && <ManualComments data={data} />);
};

export default Comments;

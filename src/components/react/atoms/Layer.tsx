import type { ReactNode } from 'react';

type Props = {
	children: ReactNode;
}

const Layer = ({ children }: Props) => (
	<div className='layer'>
		{children}
	</div>
);

export default Layer;

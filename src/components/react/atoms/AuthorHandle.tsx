import type Props from 'sukopon/types/components/atoms/AuthorHandle';

const AuthorHandle = ({ author }: Props) => (
	<a href={author.link} className='author-handle'>
		{author.handle}
	</a>
);

export default AuthorHandle;

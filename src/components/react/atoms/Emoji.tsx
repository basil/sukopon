import type Props from 'sukopon/types/components/atoms/Emoji';

const Emoji = ({ emoji }: Props) => (
	<img
		className='emoji'
		src={emoji.url}
		alt={emoji.shortcode}
		title={emoji.shortcode}
	/>
);

export default Emoji;

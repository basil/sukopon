type Context = {
	descendants: {
		id: string;
		url: string;
		in_reply_to_id: string;
		sensitive: boolean;
		spoiler_text: string;
		created_at: string;
		edited_at: string | null;
		reblogs_count: number;
		favourites_count: number;
		content: string;
		account: {
			acct: string;
			username: string;
			display_name: string;
			url: string;
			avatar: string;
			avatar_static: string;
			emojis: {
				shortcode: string;
				url: string;
				static_url: string;
			}[]
		}
	}[];
}

export default Context;
import AuthorHandle from 'sukopon/react/atoms/AuthorHandle';
import AuthorName from 'sukopon/react/atoms/AuthorName';
import Avatar from 'sukopon/react/atoms/Avatar';
import Date from 'sukopon/react/atoms/Date';

import type Props from 'sukopon/types/components/Author';

const Author = ({ comment }: Props) => (
	<header className='author'>
		<Avatar author={comment.author} />

		<div className='author-text'>
			<AuthorName author={comment.author} />

			<div className='author-subheader'>
				<AuthorHandle author={comment.author} />
				<Date comment={comment} />
			</div>
		</div>
	</header>
);

export default Author;

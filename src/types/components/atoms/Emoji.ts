import type Emoji from 'sukopon/types/emoji';

export default interface Props {
	emoji: Emoji;
}

import Emoji from 'sukopon/solid/atoms/Emoji.solid';

import type Props from 'sukopon/types/components/atoms/AuthorName';

const AuthorName = ({ author, showEmojis = true }: Props) => (
	<a href={author.link} class='author-name-link'>
		<h2 class='author-name'>
			{author.name}
			{showEmojis && author.emojis.map((emoji) => <Emoji emoji={emoji} />)}
		</h2>
	</a>
);

export default AuthorName;

import type Emoji from './emoji';

type Author = {
	name: string;
	handle: string;
	link: string;
	avatar: string;
	emojis: Emoji[];
};

export default Author;

Sukopon is a composable and customizable toolkit for making embedded [fediverse](https://en.wikipedia.org/wiki/Fediverse) comments.

Here's a quick example:

```astro
---
import Comments from 'sukopon/astro';
import 'sukopon/themes/default.scss';
---

<Comments status='110112984222031753' options={{
	instance: 'tech.lgbt'
}} />
```

In the example above, the default `Comments` component and theme are used. It requires you to install the SCSS preprocessor, which you can do via `pnpm astro add scss` for Astro projects.

---

# Prerequisites

You will have to add some of the following (whichever ones you require) to `tsconfig.json`. You will also have to copy the `src` folder in this repo to `src/lib/sukopon` in your project. You can change the destination path, just make sure to reflect those changes in the configuration.

```json
{
	"compilerOptions": {
		"baseUrl": ".",
		"paths": {
			"sukopon/*": ["./src/lib/sukopon/*"],
			"sukopon/astro": ["./src/lib/sukopon/components/astro/Comments.astro"],
			"sukopon/react": ["./src/lib/sukopon/components/react/Comments.tsx"],
			"sukopon/solid": ["./src/lib/sukopon/components/solid/Comments.solid.tsx"],
			"sukopon/astro/*": ["./src/lib/sukopon/components/astro/*"],
			"sukopon/react/*": ["./src/lib/sukopon/components/react/*"],
			"sukopon/solid/*": ["./src/lib/sukopon/components/solid/*"]
		}
	}
}
```

# Frameworks

Sukopon has an identical set of components for [Astro](https://astro.build), [React](https://react.dev), and [Solid](https://solidjs.com) (`sukopon/astro`, `sukopon/react`, and `sukopon/solid` respectively), allowing you to use the library outside of just Astro projects.

All components have the same behavior, but there is one exception; the Astro package renders comments server-side, while all of the others fetch the data client-side. There are likely ways to make the Astro component to render client-side through Astro configuration options, but that is out of the scope of this documentation.

Effectively, this means that when you're using the default component for the non-Astro frameworks, you must [hydrate the component with one of the `client:` directives](https://docs.astro.build/en/core-concepts/framework-components/#hydrating-interactive-components) (For example, `client:load` to load the component when the page itself finishes loading).

# Usage

You can customize Sukopon by writing your own styles, building your own components, or both.

## Styling

The simplest way to change the look is to build your own theme. For this, you can refer to the classes in the components as well as the default theme.

## Custom Components

If you don't like the component structure Sukopon gives you, you can make your own components. Every component is as minimal as possible. Here's an example of what the default `<Comments />` component for Astro would look like if you extracted it into as few separate components as possible:

```astro
---
import ManualComments from 'sukopon/astro/ManualComments.astro';
// Sukopon handles data fetching for you
import getComments from 'sukopon/api/mastodon/getComments';
// You may want to include your theme for the component here
import 'sukopon/themes/default.scss';
import type Props from 'sukopon/types/components/Comments';

const { status, options } = Astro.props;
const data = await getComments(status, options);
---

<div class='comments'>
	{data.comments.map((comment) => (
		<div class='comment'>
			<Author comment={comment} />
			<Content comment={comment} />
			<NestedComments comments={comment.comments} />
		</div>
	))}
</div>

<a class='original-post' href={data.link}>View original post</a>
```

You're encouraged to clone and copy the components into your own projects.

---

This library is heavily inspired by [Volpeon's fediverse comments](https://volpeon.ink/notebook/design-test) (at the bottom of the page).

For those curious, the name comes from the character <a href='https://patapon.fandom.com/wiki/Sukopon'>Sukopon</a> from the game Patapon 3 (a game which I have never beaten, but I have fond memories of the series).

import type Props from 'sukopon/types/components/atoms/Date';

const Date = ({ comment }: Props) => (
	<a href={comment.url} className='date'>
		<time dateTime={comment.date.toISOString()}>
			{comment.date.toLocaleDateString()}
		</time>
	</a>
);

export default Date;

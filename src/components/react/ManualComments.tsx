import Comment from 'sukopon/react/Comment';

import type Props from 'sukopon/types/components/ManualComments';

const ManualComments = ({ data }: Props) => (
	<>
		<div className='comments'>
			{data.comments.map((comment) =>
				<Comment
					key={comment.id} comment={comment}
				/>
			)}
		</div>
		<a className='original-post' href={data.link}>View original post</a>
	</>
);

export default ManualComments;

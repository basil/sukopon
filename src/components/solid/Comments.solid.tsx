import { createResource, Show } from 'solid-js';
import getComments from 'sukopon/api/mastodon/getComments';
import ManualComments from 'sukopon/solid/ManualComments.solid';

import type Props from 'sukopon/types/components/Comments';

const Comments = ({ status, options }: Props) => {
	const [data] = createResource(status, id => getComments(id, options));

	return (<Show when={!!data()}>
		<ManualComments data={data()} />
	</Show>);
};

export default Comments;

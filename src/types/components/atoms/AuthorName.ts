import type Author from 'sukopon/types/author';

export default interface Props {
	author: Author;
	showEmojis?: boolean;
}

import Emoji from 'sukopon/react/atoms/Emoji';

import type Props from 'sukopon/types/components/atoms/AuthorName';

const AuthorName = ({ author, showEmojis = true }: Props) => (
	<a href={author.link} className='author-name-link'>
		<h2 className='author-name'>
			{author.name}
			{showEmojis && author.emojis.map((emoji) =>
				<Emoji key={emoji.shortcode} emoji={emoji} />
			)}
		</h2>
	</a>
);

export default AuthorName;

// Emoji in username
type Emoji = {
	shortcode: string;
	url: string;
}

export default Emoji;

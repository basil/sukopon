import { getComponentOutput } from 'astro-component-tester';

/**
 * Set `document.body` to the output of an Astro component
 * @param path Path from root to module, i.e. `./src/astro/atoms/Emoji.astro`
 * @param props An object with the props that should be passed to the component
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const initAstroComponent = async (path: string, props: Record<string, any> = {}) => {
	const component = await getComponentOutput(path, props);
	document.body.innerHTML = component.html;
	return component.html;
};

export default initAstroComponent;

# Sukopon

Sukopon is a library for embedding (static) [fediverse](https://en.wikipedia.org/wiki/Fediverse) comments into a webpage.

> **NOTE**: Sukopon is not published to NPM. It is not "complete" in the sense of being neatly wrapped in a bow. It should not be hard to use, but you'll have to go more out of your way than usual to install it.
>
> For the half-finished usage document I had prepared, see the `USAGE.md` file.

## Example

```astro
---
import Comments from 'sukopon/astro';
---

<Comments status='110112984222031753' options={{
	instance: 'tech.lgbt'
}} />
```

Check out [basil.cafe/projects/sukopon](https://basil.cafe/projects/sukopon) for more information.

## Installation

In addition to installing the `sukopon` package from npm, you must add something to your `tsconfig.json`:

```json
{
	"compilerOptions": {
		"moduleResolution": "nodenext"
	}
}
```

<!--
This is due to TypeScript only recently adding support in 4.7.
https://stackoverflow.com/a/70020984
-->

## Support

### Components

- Astro
- React
- Solid

### APIs

- Mastodon-based APIs

### Caveats

- No support for:
	- images or media
	- content warnings
	- favorite/like/star or boost/repeat counts
- No testing (it's a \[mostly] static component library)

